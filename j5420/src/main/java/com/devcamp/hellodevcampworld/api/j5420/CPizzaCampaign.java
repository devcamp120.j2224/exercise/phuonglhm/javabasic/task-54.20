package com.devcamp.hellodevcampworld.api.j5420;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class CPizzaCampaign {
	@CrossOrigin
	@GetMapping("/devcamp-simple")
	public String simple() {
		return "Hello Pizza Light! application";
    }

    @CrossOrigin
    @GetMapping("/devcamp-date/{name}")
	public String getDateViet(@PathVariable String name) {
		DateTimeFormatter dtfVietNam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("Hello " + name + "! Hôm nay %s, mua 1 tặng 1!!", dtfVietNam.format(today));
	}

	@CrossOrigin
	@GetMapping("/devcamp-lucky-number")
	public String getLuckyNumber(@RequestParam(name = "username") String username,
								@RequestParam(name = "firstname") String firstname,
								@RequestParam(name = "lastname") String lastname) {
		int randomIntNumber = 1 + (int) (Math.random() * (6 - 1 +1 ));
		return String.format("Xin chào %s! Số may mắn hôm nay của bạn là %s !!!", username, randomIntNumber);
		}
	}

