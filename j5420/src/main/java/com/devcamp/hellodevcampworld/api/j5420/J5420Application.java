package com.devcamp.hellodevcampworld.api.j5420;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5420Application {

	public static void main(String[] args) {
		SpringApplication.run(J5420Application.class, args);
	}

}
